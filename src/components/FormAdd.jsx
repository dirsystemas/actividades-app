import React, { Component } from 'react';
class FormAdd extends Component {
    constructor() {
        super();
        this.handleChange = this.handleChange.bind(this);
        this.submitHandler = this.submitHandler.bind(this);
       
      }

      state = {
        inputField: '',
        inputComment:''
      }

      submitHandler(e) {
        e.preventDefault();
        // pass the input field value to the event handler passed
        // as a prop by the parent (App)
        
        console.log('actividad inputField:' + this.state.inputField)  
        this.props.handlerFromParant( {
            inputField:this.state.inputField, 
            inputComment: this.state.inputComment
        } )
        
        this.setState({
          inputField: '' ,
          inputComment:''
        })
      }
      
      handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value //inputField: e.target.value
        });
      }  

    
    render() { 
        return ( 
            <form  onSubmit={this.submitHandler} >
              <div className="form-group" style={{fontWeight:"bold", fontSize:"16px",textAlign:"center"}}>
                    Ingresar Nueva Actividad
               </div>
                <div className="form-group">
                    <label htmlFor='inputField'  style={{fontWeight:"bold",  marginRight:20}}>Actividad:</label>
                    <input name='inputField' 
                            id='inputField' 
                            value={this.state.inputField} 
                             onChange={this.handleChange}
                             type='text' 
                             placeholder='Ingrese actividad'></input>
                 </div>
                 <div className="form-group">    
                    <label  htmlFor='inputComment'  style={{fontWeight:"bold",  marginRight:5, verticalAlign:"top"}}>Comentario:</label>
                    <textarea 
                        name='inputComment' 
                        id='inputComment' 
                        value={this.state.inputComment} 
                        onChange={this.handleChange}></textarea>
                </div>
                 <button type='submit' className='btn btn-primary'>Guardar</button>
              </form>
         );
    }
}
 
export default FormAdd;