import React, { Component } from 'react';


class CardDiv  extends Component {
    state = {  }
    render() { 
        return ( 
            <div className='card'  > {this.props.children}</div>
         );
    }
}

const styles= {
    card: {
        margin: '0 auto',
        
        width: '300px',
        padding:15,
        border:'1px solid #ccc',
        borderRadius: '15',
        boxShadow:'0,0,3px,#575757',
        backgroundColor:'#c1c1c1'
    }
}


 
export default CardDiv
;