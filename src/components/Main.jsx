import React, { Component } from 'react';

class Main extends Component {
    state = { 
   
    }

   
              
    render() { 

        return (
        
         
        <React.Fragment>
            <h4> Actividad pendiente seleccionada</h4>
            <p className="text-justify" style={{margin: '5px'}}> {this.props.actividad.value} </p>
            <p className="text-justify" style={{margin: '5px'}}>{this.props.actividad.comment} </p>
            <button 
                onClick = { () => this.props._onDelete(this.props.actividad.id) } /*referenciado a counters props */
                className = 'btn btn-danger btn-sm m-2'  
                style={ { display: this._displayButton() } } >Eliminar</button> 
        </React.Fragment>
        )
    }

    
    _displayButton(){
        const {id} = this.props.actividad
        return  id === 0 ?  'none' : 'block'
    }
}

export default Main;