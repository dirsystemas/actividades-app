import React, { Component } from 'react';
import logo from '../img/react-node.png';

class BottomBar  extends Component {
    state = { 

     }

     styles = {
       // position: 'absolute',
        width:'100%',
        fontSize: '14px',
        fontWeight: 'bold',
        background: '#000',
        color: '#fff',
      //  bottom:'10px'
    }
    render() { 
        return ( 
            <div  style={this.styles}>
                <div >
                <img src={logo} width='80px' alt="logo" />
                    <span style={{paddingLeft:'30px'}}>Total de actividades pendientes: {this.props.totalActividades}</span>
                </div>
             </div>
         );
    }
}
 
export default BottomBar
;

