import React, { Component } from 'react';
import Actividad from './Actividad'

class Aside extends Component {
    state = { 
       
     }

              
    render() { 

        console.log('Actividad - Rendered');
        //Destructuring arguments { actividades, _onChange} 
       const { _onChange} = this.props;

        return (
            
        <React.Fragment>
             <h4>  Lista de actividades pendientes  </h4>
             <ul>
             { this.props.actividades.map( actividad =>( 
                <Actividad 
                    actividad = {actividad}
                    _onChange = {_onChange}
                />
                ) ) }

               
             </ul>
 
        </React.Fragment>
        )
    }
}

export default Aside;