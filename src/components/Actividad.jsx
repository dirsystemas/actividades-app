import React, { Component } from 'react';
class Actividad extends Component {
    state = { 
        name: this.props.actividad
     }


    render() { 
        console.log('Actividad - Rendered ' );
        return ( 
          <li key={this.props.actividad.id} >
            <a href='#' onClick = { () => this.props._onChange(this.props.actividad) }> 
                {this.props.actividad.value} 
            </a>
          </li> 
         );
    }
}
 
export default Actividad;