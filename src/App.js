import React, { Component } from 'react';
import NavBar from './components/NavBar'
import Actividades from './components/Actividades'
import Main from './components/Main'
import BottomBar from './components/BottomBar'
import CardDiv from './components/CardDiv'
import FormAdd from './components/FormAdd'

class Container extends Component {
    constructor() {
        super();
        this.handleData = this.handleData.bind(this);
        
      }

    state = { 
        actividades: [
            {id:1, value: 'Envio de comprobaciones pagos 10 de octubre 2016.', comment:'Enviado por Jefe de area'},
            {id:2, value: 'Administracion de base de datos.', comment:'Nota 2'},
            {id:3, value: 'Lenguajes, automatas y complejidad.', comment:'Nota 3'},
            {id:4, value: 'Sistemas de informacion.', comment:'Nota 4'},
            {id:5, value: 'Reconocimiento de patrones.', comment:'Nota 5'},
            {id:6, value: 'Analisis y diseño orientado a objetos', comment:'Nota 6'},
            {id:7, value: 'Oficio enviado Transparencia y acceso a la informacion publica.', comment:'Nota 7'},
            {id:8, value: 'Oficio de Desarrollo Humano. ', comment:'Nota 8'},
            {id:9, value: 'Curso de ingles 8 niveles. (Informar )', comment:'Nota 9'}
        ],

        actividad: {
            id:0, 
            value: '',
            comment:''
        },

        message:''
     }

    styles = {
        width: '100%'
    }

    _actividad= {
        id:0, 
        value: ''
    }

     ///llamado desde Actividad.jsx 
     _handleChange = (actividad) => {
        console.log('Event handler called', actividad.id);
        this.setState({ actividad });//this.setState({actividad: actividad});
  
    }

     ///llamado desde Main.jsx 
    _handleDelete = (actividadId) => {
    console.log('Event handler called', actividadId);
    const actividades = this.state.actividades.filter( c => c.id !== actividadId); 
    this.setState({ actividades });//this.setState({actividades: actividades});
    this.setState({actividad: this._actividad});
    }

    

    handleData(data) {
      
        const numAct = this.state.actividades.length +1
     
        const isOnList = this.state.actividades.filter( c => c.value === data.inputField);
        console.log('isOnList='+isOnList.length)

        if( isOnList.length > 0){
            this.setState({
                message: 'Este valor ya se encuentra en la lista de actividades'
            })
        }else{
            data.inputField !== '' && this.setState({
                actividades: [...this.state.actividades, {id:numAct,value:data.inputField, comment:data.inputComment}],
                message: ''
                })
        }
        /*
        this.setState({
            message: data.inputField
        });
        */
      }
    
    render() { 
        const {actividades, message} = this.state
        return ( 
            <React.Fragment>
                <NavBar /> 
                <CardDiv> 
                    <FormAdd handlerFromParant={this.handleData}/>
                    
                        {
                            message!=='' && <p className='message text-danger'>{message}</p>
                        }   

                </CardDiv>
                

                <main className='container' style={this.styles}>
                    <div className="row " >
                        <div className="col-5" >
                            <Actividades 
                                actividades = {this.state.actividades}
                                _onChange = {this._handleChange}
                            />
                        </div>
                        <div className="col-1" >
                        </div>
                        <div className="col-6" >
                            <Main 
                                actividad = {this.state.actividad}
                                _onDelete = {this._handleDelete}
                            />
                        </div>
                    </div>
                </main>
                <BottomBar totalActividades = { this.state.actividades.length} />
            </React.Fragment>
         );
    }
}
 
export default Container;